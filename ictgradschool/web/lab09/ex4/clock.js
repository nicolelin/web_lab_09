/**
 * Created by ylin183 on 5/04/2017.
 */

var tick;

// clock
function displayClock() {
    var date = new Date(); // get date
    var hour = date.getHours(); // get hours
    var min = date.getMinutes(); // get minutes
    var sec = date.getSeconds(); // get seconds

    if (hour < 10) {
        // Add the "0" digit to the front
        // so 9 becomes "09"
        hour = "0" + hour;
    }

    if (min < 10) {
        // Add the "0" digit to the front
        // so 9 becomes "09"
        min = "0" + min;
    }

    if (sec < 10) {
        // Add the "0" digit to the front
        // so 9 becomes "09"
        sec = "0" + sec;
    }

// var n = date.toDateString();
// var time = date.toLocaleTimeString();

// get clock element in html
    var clock = document.getElementById('clock');
// print text inside div
    clock.innerHTML = hour + ":" + min + ":" + sec;
}

// start clock
// repeat function every x ms
function startClock() {
    tick = setInterval(displayClock, 1000);
}

// stop clock
// reset time interval
function stopClock() {
    clearInterval(tick);
}