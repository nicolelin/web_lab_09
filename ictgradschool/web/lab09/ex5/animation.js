/**
 * Created by ylin183 on 5/04/2017.
 */

// select bauble by class and set as global variable
var baubles = document.getElementsByClassName('bauble');
console.log(baubles);

var animate = "animation: bounce 3s ease; animation-fill-mode: forwards;";
// var animateout = "animation: none;";

function onMouseOver(i) {
    var func = function(event) {
        console.log(i);
        console.log(event);
        baubles[i].style.cssText = animate;
        baubles[i].title = "";
    };

    console.log(func);

    return func;
}


// function onMouseOut(i) {
//     var func = function(event) {
//         console.log(i);
//         console.log(event);
//         baubles[i].style.cssText = animateout;
//     };
//
//     console.log(func);
//
//     return func;
// }


for(var i = 0; i < baubles.length; i++) {
    baubles[i].onmouseover = onMouseOver(i);
    // baubles[i].onmouseout = onMouseOut(i);
}