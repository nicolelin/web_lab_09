// JSON DATA //
var customers = [
	{ "name" : "Peter Jackson", "gender" : "male", "year_born" : 1961, "joined" : "1997", "num_hires" : 17000 },
		
	{ "name" : "Jane Campion", "gender" : "female", "year_born" : 1954, "joined" : "1980", "num_hires" : 30000 },
	
	{ "name" : "Roger Donaldson", "gender" : "male", "year_born" : 1945, "joined" : "1980", "num_hires" : 12000 },
	
	{ "name" : "Temuera Morrison", "gender" : "male", "year_born" : 1960, "joined" : "1995", "num_hires" : 15500 },
	
	{ "name" : "Russell Crowe", "gender" : "male", "year_born" : 1964, "joined" : "1990", "num_hires" : 10000 },
	
	{ "name" : "Lucy Lawless", "gender" : "female", "year_born" : 1968, "joined" : "1995", "num_hires" : 5000 },	
		
	{ "name" : "Michael Hurst", "gender" : "male", "year_born" : 1957, "joined" : "2000", "num_hires" : 15000 },
		
	{ "name" : "Andrew Niccol", "gender" : "male", "year_born" : 1964, "joined" : "1997", "num_hires" : 3500 },	
	
	{ "name" : "Kiri Te Kanawa", "gender" : "female", "year_born" : 1944, "joined" : "1997", "num_hires" : 500 },	
	
	{ "name" : "Lorde", "gender" : "female", "year_born" : 1996, "joined" : "2010", "num_hires" : 1000 },	
	
	{ "name" : "Scribe", "gender" : "male", "year_born" : 1979, "joined" : "2000", "num_hires" : 5000 },

	{ "name" : "Kimbra", "gender" : "female", "year_born" : 1990, "joined" : "2005", "num_hires" : 7000 },
	
	{ "name" : "Neil Finn", "gender" : "male", "year_born" : 1958, "joined" : "1985", "num_hires" : 6000 },	
	
	{ "name" : "Anika Moa", "gender" : "female", "year_born" : 1980, "joined" : "2000", "num_hires" : 700 },
	
	{ "name" : "Bic Runga", "gender" : "female", "year_born" : 1976, "joined" : "1995", "num_hires" : 5000 },
	
	{ "name" : "Ernest Rutherford", "gender" : "male", "year_born" : 1871, "joined" : "1930", "num_hires" : 4200 },
	
	{ "name" : "Kate Sheppard", "gender" : "female", "year_born" : 1847, "joined" : "1930", "num_hires" : 1000 },
	
	{ "name" : "Apirana Turupa Ngata", "gender" : "male", "year_born" : 1874, "joined" : "1920", "num_hires" : 3500 },
	
	{ "name" : "Edmund Hillary", "gender" : "male", "year_born" : 1919, "joined" : "1955", "num_hires" : 10000 },
	
	{ "name" : "Katherine Mansfield", "gender" : "female", "year_born" : 1888, "joined" : "1920", "num_hires" : 2000 },
	
	{ "name" : "Margaret Mahy", "gender" : "female", "year_born" : 1936, "joined" : "1985", "num_hires" : 5000 },
	
	{ "name" : "John Key", "gender" : "male", "year_born" : 1961, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Sonny Bill Williams", "gender" : "male", "year_born" : 1985, "joined" : "1995", "num_hires" : 15000 },
	
	{ "name" : "Dan Carter", "gender" : "male", "year_born" : 1982, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Bernice Mene", "gender" : "female", "year_born" : 1975, "joined" : "1990", "num_hires" : 30000 }	
];


// // BUILD HTML TABLE // //
function CreateJSONTable() {

// GET VALUE FOR TABLE HEADER
var col = [];
for (var i = 0; i < customers.length; i++) {
    for (var key in customers[i]) {
        if (col.indexOf(key) === -1) {
            col.push(key);
            // console.log (key);
        }
    }
}

// CREATE TABLE
var table = document.createElement("table");

// CREATE HTML TABLE HEADER USING KEY VALUES FROM ABOVE

var tr = table.insertRow(-1);                   // TABLE ROW.

for (var i = 0; i < col.length; i++) {
    var th = document.createElement("th");      // TABLE HEADER.
    th.innerHTML = col[i];
    tr.appendChild(th);
}

// APPEND JSON VALUES TO TABLE
for (var i = 0; i < customers.length; i++) {
    tr = table.insertRow(-1);
    for (var j = 0; j < col.length; j++) {
        var tabCell = tr.insertCell(-1);
        tabCell.innerHTML = customers[i][col[j]];
    }
}

// ADD GENERATED TABLE TO CONTAINER
var divContainer = document.getElementById("getData");
divContainer.innerHTML = "";
divContainer.appendChild(table);

}

// // BUILD STATS TABLE // //
function CreateStatsTable() {

    // Initialise stats variables
    var counterM = 0;
    var counterF = 0;

    var counterAge0_30 = 0;
    var counterAge31_64 = 0;
    var counterAge65_above = 0;

    var counterGold = 0;
    var counterSilver = 0;
    var counterBronze = 0;

    // Build Stats
    for (var i = 0; i < customers.length; i++) {

        // gender counter
        if (customers[i].gender === "male") {
            counterM++;
        } else if (customers[i].gender === "female") {
            counterF++;
        }

        // age group counter
        if ((2017 - customers[i].year_born) > 0 && (2017 - customers[i].year_born) <= 30) {
            counterAge0_30++;
        } else if ((2017 - customers[i].year_born) >= 31 && (2017 - customers[i].year_born) <= 64) {
            counterAge31_64++;
        } else if ((2017 - customers[i].year_born) >= 65) {
            counterAge65_above++;
        }

        var avgHiresPerYear = customers[i].num_hires / ((2017 - customers[i].joined) * 52);

        // loyalty counter
        if (avgHiresPerYear > 4) {
            counterGold++;
        } else if (avgHiresPerYear < 4 && avgHiresPerYear >= 1) {
            counterSilver++;
        } else if (avgHiresPerYear < 1) {
            counterBronze++;
        }

    }

    // DEBUG
    // console.log("counterM " + counterM);
    // console.log("counterF " + counterF);
    // console.log("counterAge0_30 " + counterAge0_30);
    // console.log("counterAge31_64 " + counterAge31_64);
    // console.log("counterAge65_above " + counterAge65_above);
    // console.log("counterGold " + counterGold);
    // console.log("counterSilver " + counterSilver);
    // console.log("counterBronze " + counterBronze);

    // Generate table

    var table = document.createElement("table");

    // create <th> row
    var row0 = table.insertRow(0);
    var cell0_1 = row0.insertCell(0);
    var cell0_2 = row0.insertCell(1);
    cell0_1.innerHTML = "Stats";
    cell0_2.innerHTML = "# of Customers";

    // create empty <tr> rows
    var row1 = table.insertRow(1);
    var row2 = table.insertRow(2);
    var row3 = table.insertRow(3);
    var row4 = table.insertRow(4);
    var row5 = table.insertRow(5);
    var row6 = table.insertRow(6);
    var row7 = table.insertRow(7);
    var row8 = table.insertRow(8);

    // Insert cells into position 1 and 2 of <tr> rows
    // Row 1
    var cell1_1 = row1.insertCell(0);
    var cell1_2 = row1.insertCell(1);
    // Row 2
    var cell2_1 = row2.insertCell(0);
    var cell2_2 = row2.insertCell(1);
    // Row 3
    var cell3_1 = row3.insertCell(0);
    var cell3_2 = row3.insertCell(1);
    // Row 4
    var cell4_1 = row4.insertCell(0);
    var cell4_2 = row4.insertCell(1);
    // Row 5
    var cell5_1 = row5.insertCell(0);
    var cell5_2 = row5.insertCell(1);
    // Row 6
    var cell6_1 = row6.insertCell(0);
    var cell6_2 = row6.insertCell(1);
    // Row 7
    var cell7_1 = row7.insertCell(0);
    var cell7_2 = row7.insertCell(1);
    // Row 8
    var cell8_1 = row8.insertCell(0);
    var cell8_2 = row8.insertCell(1);

    // Insert content into new cells
    // Row 1
    cell1_1.innerHTML = "Gender: Male";
    cell1_2.innerHTML = counterM;
    // Row 2
    cell2_1.innerHTML = "Gender: Female";
    cell2_2.innerHTML = counterF;
    // Row 3
    cell3_1.innerHTML = "Age Range: 0 to 30";
    cell3_2.innerHTML = counterAge0_30;
    // Row 4
    cell4_1.innerHTML = "Age Range: 31 to 64";
    cell4_2.innerHTML = counterAge31_64;
    // Row 5
    cell5_1.innerHTML = "Age Range: 65 and above";
    cell5_2.innerHTML = counterAge65_above;
    // Row 6
    cell6_1.innerHTML = "Loyalty Status: Gold";
    cell6_2.innerHTML = counterGold;
    // Row 7
    cell7_1.innerHTML = "Loyalty Status: Silver";
    cell7_2.innerHTML = counterSilver;
    // Row 8
    cell8_1.innerHTML = "Loyalty Status: Bronze";
    cell8_2.innerHTML = counterBronze;

    // Add stats to table
    var divContainer = document.getElementById("getStats");
    divContainer.innerHTML = "";
    divContainer.appendChild(table);

}


