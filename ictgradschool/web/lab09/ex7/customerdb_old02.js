var customers = [
	{ "name" : "Peter Jackson", "gender" : "male", "year_born" : 1961, "joined" : "1997", "num_hires" : 17000 },
		
	{ "name" : "Jane Campion", "gender" : "female", "year_born" : 1954, "joined" : "1980", "num_hires" : 30000 },
	
	{ "name" : "Roger Donaldson", "gender" : "male", "year_born" : 1945, "joined" : "1980", "num_hires" : 12000 },
	
	{ "name" : "Temuera Morrison", "gender" : "male", "year_born" : 1960, "joined" : "1995", "num_hires" : 15500 },
	
	{ "name" : "Russell Crowe", "gender" : "male", "year_born" : 1964, "joined" : "1990", "num_hires" : 10000 },
	
	{ "name" : "Lucy Lawless", "gender" : "female", "year_born" : 1968, "joined" : "1995", "num_hires" : 5000 },	
		
	{ "name" : "Michael Hurst", "gender" : "male", "year_born" : 1957, "joined" : "2000", "num_hires" : 15000 },
		
	{ "name" : "Andrew Niccol", "gender" : "male", "year_born" : 1964, "joined" : "1997", "num_hires" : 3500 },	
	
	{ "name" : "Kiri Te Kanawa", "gender" : "female", "year_born" : 1944, "joined" : "1997", "num_hires" : 500 },	
	
	{ "name" : "Lorde", "gender" : "female", "year_born" : 1996, "joined" : "2010", "num_hires" : 1000 },	
	
	{ "name" : "Scribe", "gender" : "male", "year_born" : 1979, "joined" : "2000", "num_hires" : 5000 },

	{ "name" : "Kimbra", "gender" : "female", "year_born" : 1990, "joined" : "2005", "num_hires" : 7000 },
	
	{ "name" : "Neil Finn", "gender" : "male", "year_born" : 1958, "joined" : "1985", "num_hires" : 6000 },	
	
	{ "name" : "Anika Moa", "gender" : "female", "year_born" : 1980, "joined" : "2000", "num_hires" : 700 },
	
	{ "name" : "Bic Runga", "gender" : "female", "year_born" : 1976, "joined" : "1995", "num_hires" : 5000 },
	
	{ "name" : "Ernest Rutherford", "gender" : "male", "year_born" : 1871, "joined" : "1930", "num_hires" : 4200 },
	
	{ "name" : "Kate Sheppard", "gender" : "female", "year_born" : 1847, "joined" : "1930", "num_hires" : 1000 },
	
	{ "name" : "Apirana Turupa Ngata", "gender" : "male", "year_born" : 1874, "joined" : "1920", "num_hires" : 3500 },
	
	{ "name" : "Edmund Hillary", "gender" : "male", "year_born" : 1919, "joined" : "1955", "num_hires" : 10000 },
	
	{ "name" : "Katherine Mansfield", "gender" : "female", "year_born" : 1888, "joined" : "1920", "num_hires" : 2000 },
	
	{ "name" : "Margaret Mahy", "gender" : "female", "year_born" : 1936, "joined" : "1985", "num_hires" : 5000 },
	
	{ "name" : "John Key", "gender" : "male", "year_born" : 1961, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Sonny Bill Williams", "gender" : "male", "year_born" : 1985, "joined" : "1995", "num_hires" : 15000 },
	
	{ "name" : "Dan Carter", "gender" : "male", "year_born" : 1982, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Bernice Mene", "gender" : "female", "year_born" : 1975, "joined" : "1990", "num_hires" : 30000 }	
];



// Initialise variables for creating new elements
var table = document.createElement('table'),
    tr = document.createElement('tr'),
    th = document.createElement('th'),
    td = document.createElement('td');

// Table variables
var name = "";
var gender = "";
var year_born = 0;
var joined = 0;
var num_hires = 0;

// Stats variables
var counterM = 0;
var counterF = 0;

var counterAge0_30 = 0;
var counterAge31_64 = 0;
var counterAge65_ = 0;

var counterGold = 0;
var counterSilver = 0;
var counterBronze = 0;

// Loop through JSON
// for (var i = 0; i < customers.length; i++) {

var htmlText = "";

for (var key in customers) {

    // Build HTML table





    // Build Stats

    // gender counter
    if (customers[i].gender === "male") {
        counterM++;
    } else if (customers[i].gender === "female") {
        counterF++;
    }

    // age group counter
    if ((2017 - customers[i].year_born) > 0 && (2017 - customers[i].year_born) <= 30) {
        counterAge0_30++;
    } else if ((2017 - customers[i].year_born) >= 31 && (2017 - customers[i].year_born) <= 64) {
        counterAge31_64++;
    } else if ((2017 - customers[i].year_born) >= 65) {
        counterAge65_++;
    }

    var avgHiresPerYear = customers[i].num_hires/((2017-customers[i].joined) * 52);

    // loyalty counter
    if (avgHiresPerYear > 4) {
        counterGold++;
    } else if (avgHiresPerYear < 4 && avgHiresPerYear >= 1) {
        counterSilver++;
    } else if (avgHiresPerYear < 1) {
        counterBronze++;
    }

    // Print Stats


}

// Debug

console.log("counterM " + counterM);
console.log("counterF " + counterF);
console.log("counterAge0_30 " + counterAge0_30);
console.log("counterAge31_64 " + counterAge31_64);
console.log("counterAge65_ " + counterAge65_);
console.log("counterGold " + counterGold);
console.log("counterSilver " + counterSilver);
console.log("counterBronze " + counterBronze);