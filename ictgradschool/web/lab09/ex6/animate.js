/**
 * Created by ylin183 on 11/04/2017.
 */

var pages = document.getElementsByClassName('page');
// console.log(pages);

function onMouseClick(i) {
    var func = function(event) {
        // console.log(i);
        // console.log(event);
        pages[i].classList.add('pageAnimation');
    };

    // console.log(func);

    return func;
}

function onAnimEnd(i) {
    var func = function(event) {
        // console.log(i);
        // console.log(event);
        if (i+1 < pages.length) {
            pages[i+1].style.zIndex = 1;
        }
    };

    // console.log(func);

    return func;
}

for (var i = 0; i < pages.length; i++) {
    pages[i].onclick = onMouseClick(i);
    pages[i].addEventListener("animationend", onAnimEnd(i));
}